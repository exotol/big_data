package ru.cs.vsu.mapper;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Created by maxtrav on 12.11.16.
 */
public class MaxMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    private Text keyMax = new Text();
    private IntWritable valMax = new IntWritable();
    private IntWritable newValue = new IntWritable();
    private Text newKey = new Text();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] values = value.toString().split("\t");
        newValue.set(Integer.valueOf(values[1]));
        newKey.set(values[0]);
        if(valMax.get() < newValue.get()) {
            valMax.set(newValue.get());
            keyMax.set(newKey);
        }
    }


    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        context.write(keyMax, valMax);
    }
}
