package ru.cs.vsu.reduce;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by maxtrav on 13.11.16.
 */
public class MaxReduce extends Reducer<Text, IntWritable, Text, IntWritable> {

    private Text keyMax = new Text();
    private IntWritable maxValue = new IntWritable();

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        for(IntWritable value : values) {
            if(maxValue.get() < value.get()) {
                maxValue.set(value.get());
                keyMax.set(key);
            }
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        context.write(keyMax, maxValue);
    }
}
