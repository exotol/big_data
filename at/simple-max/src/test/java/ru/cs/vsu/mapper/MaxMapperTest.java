package ru.cs.vsu.mapper;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxtrav on 12.11.16.
 */
public class MaxMapperTest {

    private MapDriver<LongWritable, Text, Text, IntWritable> newMapDriver;
    private List<String> values;

    @Before
    public void setUp() throws Exception {
        MaxMapper maxMapper = new MaxMapper();
        newMapDriver = MapDriver.newMapDriver(maxMapper);
        values = readDataFromFile();
    }

    private List<String> readDataFromFile() throws IOException {
        File file = new File(this.getClass().getResource("/ru/cs/vsu/mapper/test_output.txt").getFile());
        BufferedReader reader = new BufferedReader(new FileReader(file));
        List<String> values = new ArrayList<String>();
        String line = "";
        while ((line = reader.readLine()) != null) {
            values.add(line);
        }
        return values;
    }

    @Test
    public void testOutputMapper() throws Exception {
        int counter = 0;
        for(String value : values) {
            newMapDriver.withInput(new LongWritable(counter++), new Text(value));
        }
        newMapDriver.withOutput(new Text("Name3"), new IntWritable(1254));
        newMapDriver.run();
    }
}
