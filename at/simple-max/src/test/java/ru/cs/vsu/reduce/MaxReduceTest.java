package ru.cs.vsu.reduce;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import ru.cs.vsu.mapper.MaxMapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxtrav on 13.11.16.
 */
public class MaxReduceTest {
    private ReduceDriver<Text, IntWritable, Text, IntWritable> newReduceDriver;

    @Before
    public void setUp() throws Exception {
        MaxReduce maxReduce = new MaxReduce();
        newReduceDriver = ReduceDriver.newReduceDriver(maxReduce);
    }


    @Test
    public void testOutputMapper() throws Exception {
        List<IntWritable> intWritables1 = new ArrayList<IntWritable>();
        intWritables1.add(new IntWritable(132));
        newReduceDriver.withInput(new Text("Name1"), intWritables1);

        List<IntWritable> intWritables2 = new ArrayList<IntWritable>();
        intWritables1.add(new IntWritable(123));
        newReduceDriver.withInput(new Text("Name2"), intWritables2);

        List<IntWritable> intWritables3 = new ArrayList<IntWritable>();
        intWritables1.add(new IntWritable(1254));
        newReduceDriver.withInput(new Text("Name3"), intWritables3);

        List<IntWritable> intWritables4 = new ArrayList<IntWritable>();
        intWritables1.add(new IntWritable(143));
        newReduceDriver.withInput(new Text("Name4"), intWritables4);

        List<IntWritable> intWritables5 = new ArrayList<IntWritable>();
        intWritables1.add(new IntWritable(192));
        newReduceDriver.withInput(new Text("Name1"), intWritables5);

        newReduceDriver.withOutput(new Text("Name3"), new IntWritable(1254));
        newReduceDriver.run();
    }



}
