package ru.cs.vsu.mapper;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by maxtrav on 13.11.16.
 */
public class TopNMapper extends Mapper<Object, Text, NullWritable, Text> {
    public static final int TOP_LIST_SIZE = 3;

    private TreeMap<Long, String> clientBalanceContainer;

    public TopNMapper() {
        clientBalanceContainer = new TreeMap<Long, String>();
    }



    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        // Split on tab to get the cat name and the weight
        String tokens[] = value.toString().split(";");
        Long balance = Long.parseLong(tokens[1]);
        clientBalanceContainer.put(balance, value.toString());
        if (clientBalanceContainer.size() > TOP_LIST_SIZE) {
            clientBalanceContainer.remove(clientBalanceContainer.firstKey());
        }
    }


    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        for (String clientBalance : clientBalanceContainer.values() ) {
            context.write(NullWritable.get(), new Text(clientBalance));
        }
    }
}
