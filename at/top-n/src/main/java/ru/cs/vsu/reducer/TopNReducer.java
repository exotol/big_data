package ru.cs.vsu.reducer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import ru.cs.vsu.mapper.TopNMapper;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by maxtrav on 13.11.16.
 */
public class TopNReducer extends Reducer<NullWritable, Text, NullWritable, Text> {

    private TreeMap<Long, String> topTen;

    public TopNReducer() {
        topTen = new TreeMap<Long, String>(Collections.reverseOrder());
    }

    @Override
    protected void reduce(NullWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        for (Text value : values) {
            String tokens[] = value.toString().split(";");
            Long balance = Long.parseLong(tokens[1]);
            topTen.put(balance, value.toString());

            if (topTen.size() > TopNMapper.TOP_LIST_SIZE) {
                topTen.remove(topTen.firstKey());
            }
        }
        for (String topTenRecord : topTen.values()) {
            context.write(NullWritable.get(), new Text(topTenRecord));
        }
    }

}
