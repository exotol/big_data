package ru.cs.vsu.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxtrav on 13.11.16.
 */
public class Utils {
    public static List<String[]> readDataFromFile(Class<?> className, String fileName) throws IOException {
        File file = new File(className.getResource(fileName).getFile());
        BufferedReader reader = new BufferedReader(new FileReader(file));
        List<String[]> values = new ArrayList<String[]>();
        String line = "";
        while ((line = reader.readLine()) != null) {
            values.add(line.split("\t"));
        }
        return values;
    }

    public static List<String> readDataFromFile(Class<?> className, String fileName, boolean isMapper) throws IOException {
        File file = new File(className.getResource(fileName).getFile());
        BufferedReader reader = new BufferedReader(new FileReader(file));
        List<String> values = new ArrayList<String>();
        String line = "";
        while ((line = reader.readLine()) != null) {
            values.add(line);
        }
        return values;
    }
}
