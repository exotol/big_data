package ru.cs.vsu.metadata;

import org.apache.hadoop.io.LongWritable;
import ru.cs.vsu.entity.AbstractBaseEntity;
import ru.cs.vsu.entity.Client;
import ru.cs.vsu.key.TaggedKey;
import ru.cs.vsu.parser.CommonParser;
import ru.cs.vsu.parser.impl.ClientMetadataParser;
import ru.cs.vsu.util.ParserType;
import ru.cs.vsu.util.Parsers;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by maxtrav on 15.11.16.
 */
public class ClientMetadata {

    private Map<TaggedKey, Client> clientIdToData = new HashMap<TaggedKey, Client>();

    public void initialize(String file) throws IOException {
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))){
            String line = "";
            while ((line = reader.readLine()) != null) {
                Client client = (Client)Parsers.getParser(ParserType.CLIENT).parse(line);
                if(client != null) {
                    clientIdToData.put(client.getId(), client);
                }
            }
        }
    }

    public Client getClientInfo(TaggedKey clientId) {
        return clientIdToData.get(clientId);
    }

    public Map<TaggedKey, Client> getClientIdToData() {
        return Collections.unmodifiableMap(clientIdToData);
    }
}
