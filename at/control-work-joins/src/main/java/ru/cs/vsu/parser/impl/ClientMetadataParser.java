package ru.cs.vsu.parser.impl;

import ru.cs.vsu.entity.Client;

/**
 * Created by maxtrav on 15.11.16.
 */
public class ClientMetadataParser extends AbstractCommonParser<Client> {

    private static final ClientMetadataParser instance = new ClientMetadataParser();
    private static final int COUNT_TOKENS = 3;

    private ClientMetadataParser() {

    }

    public static ClientMetadataParser getInstance() {
        return instance;
    }

    @Override
    public Client parse(String line) {
        String[] tokens = line.split(AbstractCommonParser.PARSER_SEPARATOR);
        if(tokens.length == COUNT_TOKENS) {
            return new Client(tokens[0], tokens[1], tokens[2]);
        }
        return null;
    }
}
