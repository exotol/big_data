package ru.cs.vsu.entity;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;
import ru.cs.vsu.key.TaggedKey;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by maxtrav on 16.11.16.
 */
public abstract class AbstractBaseEntity<T, PR extends TaggedKey> implements WritableComparable<T> {

    protected static final String SEPARATOR_STRING = "";

    protected PR id;

    public PR getId() {
        return id;
    }

    public void setId(PR id) {
        this.id = id;
    }

    protected AbstractBaseEntity(PR id) {
        this.id = id;
    }


    @Override
    public int compareTo(T o) {
        return id.compareTo(((AbstractBaseEntity<T, PR>)o).id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractBaseEntity that = (AbstractBaseEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
