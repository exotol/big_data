package ru.cs.vsu.key;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * Created by maxtrav on 16.11.16.
 */
public class GroupComparator extends WritableComparator {

    public GroupComparator() {
        super(TaggedKey.class, true);
    }

    public int compare(WritableComparable a, WritableComparable b) {
        TaggedKey taggedKeyA = (TaggedKey)a;
        TaggedKey taggedKeyB = (TaggedKey)b;
        return taggedKeyA.getJoinKey().compareTo(taggedKeyB.getJoinKey());
    }
}
