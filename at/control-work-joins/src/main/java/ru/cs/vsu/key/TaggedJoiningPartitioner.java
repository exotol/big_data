package ru.cs.vsu.key;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * Created by maxtrav on 16.11.16.
 */
public class TaggedJoiningPartitioner extends Partitioner<TaggedKey, Text> {


    @Override
    public int getPartition(TaggedKey taggedKey, Text text, int numPartitions) {
        return taggedKey.getJoinKey().hashCode() % numPartitions;
    }

}
