package ru.cs.vsu.parser.impl;

import ru.cs.vsu.entity.Salary;

/**
 * Created by maxtrav on 16.11.16.
 */
public class SalaryDataParser extends AbstractCommonParser<Salary> {


    private static final SalaryDataParser instance = new SalaryDataParser();
    private static final int COUNT_TOKENS = 2;

    private SalaryDataParser() {

    }

    public static SalaryDataParser getInstance() {
        return instance;
    }

    @Override
    public Salary parse(String lineParse) {
        String[] tokens = lineParse.split(AbstractCommonParser.PARSER_SEPARATOR);
        if(tokens.length == COUNT_TOKENS) {
            return new Salary(tokens[0], tokens[1]);
        }
        return null;
    }
}
