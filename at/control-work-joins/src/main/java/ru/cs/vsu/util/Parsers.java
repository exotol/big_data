package ru.cs.vsu.util;

import ru.cs.vsu.entity.AbstractBaseEntity;
import ru.cs.vsu.entity.Department;
import ru.cs.vsu.exception.HadoopParserOperationUnsupportedException;
import ru.cs.vsu.parser.CommonParser;
import ru.cs.vsu.parser.impl.AbstractCommonParser;
import ru.cs.vsu.parser.impl.ClientMetadataParser;
import ru.cs.vsu.parser.impl.DepartmentDataParser;
import ru.cs.vsu.parser.impl.SalaryDataParser;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by maxtrav on 16.11.16.
 */
public class Parsers {

    public static CommonParser<? extends AbstractBaseEntity> getParser(ParserType type) {
        switch (type) {
            case CLIENT:
                return ClientMetadataParser.getInstance();
            case DEPARTMENT:
                return DepartmentDataParser.getInstance();
            case SALARY:
                return SalaryDataParser.getInstance();
        }
        throw new HadoopParserOperationUnsupportedException("Данный тип сущности не имеет реализации парсера!!!");
    }

}
