package ru.cs.vsu.exception;

/**
 * Created by maxtrav on 16.11.16.
 */
public class HadoopParserOperationUnsupportedException extends UnsupportedOperationException {

    public HadoopParserOperationUnsupportedException() {
    }

    public HadoopParserOperationUnsupportedException(String message) {
        super(message);
    }

    public HadoopParserOperationUnsupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public HadoopParserOperationUnsupportedException(Throwable cause) {
        super(cause);
    }
}
