package ru.cs.vsu.entity;

import org.apache.hadoop.io.LongWritable;

/**
 * Created by maxtrav on 16.11.16.
 */
public class SalaryDepartmentClient extends DepartmentClient {


    public Salary getSalary() {
        return salary;
    }

    public void setSalary(Salary salary) {
        this.salary = salary;
    }

    private Salary salary;

    private void set(Salary salary) {
        this.setSalary(salary);
    }

    public SalaryDepartmentClient(DepartmentClient departmentClient, Salary salary) {
        super(departmentClient.getClient(), departmentClient);
        set(salary);
    }
}
