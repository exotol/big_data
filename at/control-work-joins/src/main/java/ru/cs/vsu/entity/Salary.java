package ru.cs.vsu.entity;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import ru.cs.vsu.key.TaggedKey;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by maxtrav on 16.11.16.
 */
public class Salary extends AbstractBaseEntity<Salary, TaggedKey> {

    public static final long ORDER_INDEX = 2L;

    private LongWritable salary;


    private void set(LongWritable salary) {
        this.salary = salary;
    }

    public void set(Salary salary) {
        this.setId(new TaggedKey(salary.getId()));
        this.salary.set(salary.getSalary().get());
    }

    public Salary(TaggedKey id, LongWritable salary) {
        super(new TaggedKey(id));
        set(salary);
    }

    public Salary(LongWritable id, LongWritable salary) {
        super(new TaggedKey(id, new LongWritable(ORDER_INDEX)));
        set(salary);
    }

    public LongWritable getSalary() {
        return salary;
    }

    public void setSalary(LongWritable salary) {
        this.salary = salary;
    }

    public Salary(String departmentId, String salary) {
        super(new TaggedKey(new LongWritable(Long.valueOf(departmentId)), new LongWritable(ORDER_INDEX)));
        set(new LongWritable(Long.valueOf(salary)));
    }

    @Override
    public void write(DataOutput out) throws IOException {
        id.write(out);
        salary.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        id.readFields(in);
        salary.readFields(in);
    }

    @Override
    public String toString() {
        return salary.toString();
    }
}
