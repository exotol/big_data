package ru.cs.vsu.parser;

/**
 * Created by maxtrav on 15.11.16.
 */
public interface CommonParser<T> {

    T parse(String lineParse);
}
