package ru.cs.vsu.main;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import ru.cs.vsu.entity.SalaryDepartmentClient;
import ru.cs.vsu.exception.HadoopIllegalArgumentException;
import ru.cs.vsu.key.GroupComparator;
import ru.cs.vsu.key.TaggedJoiningPartitioner;
import ru.cs.vsu.mapper.FileJoinDistributeCacheMapper;
import ru.cs.vsu.mapper.FileJoinFileMapper;
import ru.cs.vsu.reducer.JoinReducer;

import java.net.URI;

/**
 * Created by maxtrav on 15.11.16.
 */
public class JoinDriver extends Configured implements Tool {

    public static final int COUNT_ARGS = 4;

    public static void main(String[] args) {
        try {
            int exitCode = ToolRunner.run(new JoinDriver(), args);
            System.exit(exitCode);
            System.out.println("Успех!!!");
        } catch (Exception exp) {
            System.err.println("Программа завершилась с ошибкой: " +exp.getMessage());
        }

    }

    @Override
    public int run(String[] args) throws Exception {
        if(args.length != COUNT_ARGS) {
            System.err.println("Формат ввода <кеш файл> <входной файл департамента> <входной файл зарплатного отдела> <выходная папка>");
            throw new HadoopIllegalArgumentException("Передано неверное число аргументов!!!");
        }
        String cacheFile = args[0];
        String inputFileDepartment = args[1];
        String inputFileSalary = args[2];
        String output = args[3];

        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, "2 Joins");
        job.setJarByClass(JoinDriver.class);
        job.addCacheFile(new URI(cacheFile));

        MultipleInputs.addInputPath(job, new Path(inputFileDepartment), TextInputFormat.class, FileJoinDistributeCacheMapper.class);
        MultipleInputs.addInputPath(job, new Path(inputFileSalary), TextInputFormat.class, FileJoinFileMapper.class);

        FileOutputFormat.setOutputPath(job, new Path(output));
        job.setReducerClass(JoinReducer.class);
        job.setPartitionerClass(TaggedJoiningPartitioner.class);
        job.setGroupingComparatorClass(GroupComparator.class);

        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(SalaryDepartmentClient.class);
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
