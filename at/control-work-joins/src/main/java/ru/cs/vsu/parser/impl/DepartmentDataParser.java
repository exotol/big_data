package ru.cs.vsu.parser.impl;

import ru.cs.vsu.entity.Department;

/**
 * Created by maxtrav on 15.11.16.
 */
public class DepartmentDataParser  extends AbstractCommonParser<Department> {

    private static final DepartmentDataParser instance = new DepartmentDataParser();
    private static final int COUNT_TOKENS = 2;

    private DepartmentDataParser() {

    }

    public static DepartmentDataParser getInstance() {
        return instance;
    }

    @Override
    public Department parse(String line) {
        String[] tokens = line.split(AbstractCommonParser.PARSER_SEPARATOR);
        if(tokens.length == COUNT_TOKENS) {
            return new Department(tokens[0], tokens[1]);
        }
        return null;
    }
}
