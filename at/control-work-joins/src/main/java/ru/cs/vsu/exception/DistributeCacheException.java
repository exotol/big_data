package ru.cs.vsu.exception;

import java.io.FileNotFoundException;

/**
 * Created by maxtrav on 15.11.16.
 */
public class DistributeCacheException extends FileNotFoundException {

    public DistributeCacheException() {
    }

    public DistributeCacheException(String message) {
        super(message);
    }
}
