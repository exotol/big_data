package ru.cs.vsu.entity;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import ru.cs.vsu.key.TaggedKey;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by maxtrav on 15.11.16.
 */
public class Client extends AbstractBaseEntity<Client, TaggedKey> {

    public static final long ORDER_INDEX = 1L;
    private Text name;
    private Text surname;



    private void set(Text name, Text surname) {
        this.name = name;
        this.surname = surname;
    }


    public Client(Client client) {
        this(client.getId(), client.getName(), client.getSurname());
    }


    public Client(TaggedKey id, Text name, Text surname) {
        super(new TaggedKey(id));
        set(name, surname);
    }

    public Client(LongWritable clientId, Text name, Text surname) {
        super(new TaggedKey(clientId, new LongWritable(ORDER_INDEX)));
        set(new Text(name), new Text(surname));
    }

    public Client(String clientId, String name, String surname) {
        super(new TaggedKey(new LongWritable(Long.valueOf(clientId)),  new LongWritable(ORDER_INDEX)));
        set(new Text(name), new Text(surname));
    }

    public Client() {
        super(new TaggedKey(new LongWritable(), new LongWritable(ORDER_INDEX)));
        set(new Text(), new Text());
    }

    public Text getName() {
        return name;
    }

    public void setName(Text name) {
        this.name = name;
    }

    public Text getSurname() {
        return surname;
    }

    public void setSurname(Text surname) {
        this.surname = surname;
    }


    @Override
    public void write(DataOutput dataOutput) throws IOException {
        id.write(dataOutput);
        name.write(dataOutput);
        surname.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        id.readFields(dataInput);
        name.readFields(dataInput);
        surname.readFields(dataInput);
    }

    @Override
    public String toString() {
        return name + AbstractBaseEntity.SEPARATOR_STRING + surname;
    }
}
