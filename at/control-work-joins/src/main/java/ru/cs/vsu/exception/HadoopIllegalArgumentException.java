package ru.cs.vsu.exception;

/**
 * Created by maxtrav on 16.11.16.
 */
public class HadoopIllegalArgumentException extends IllegalArgumentException {

    public HadoopIllegalArgumentException() {
    }

    public HadoopIllegalArgumentException(String s) {
        super(s);
    }

    public HadoopIllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public HadoopIllegalArgumentException(Throwable cause) {
        super(cause);
    }
}
