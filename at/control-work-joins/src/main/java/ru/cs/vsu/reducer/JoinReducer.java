package ru.cs.vsu.reducer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;
import ru.cs.vsu.entity.AbstractBaseEntity;
import ru.cs.vsu.entity.DepartmentClient;
import ru.cs.vsu.entity.Salary;
import ru.cs.vsu.entity.SalaryDepartmentClient;
import ru.cs.vsu.key.TaggedKey;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maxtrav on 16.11.16.
 */
public class JoinReducer extends Reducer<TaggedKey, AbstractBaseEntity, LongWritable, SalaryDepartmentClient> {
    private static final int SIZE_TWO = 2;


    @Override
    protected void reduce(TaggedKey key, Iterable<AbstractBaseEntity> values, Context context) throws IOException, InterruptedException {
        List<AbstractBaseEntity> entities = new ArrayList<AbstractBaseEntity>();
        for(AbstractBaseEntity abstractBaseEntity : values) {
            entities.add(abstractBaseEntity);
        }
        if(entities.size() == SIZE_TWO) {
            context.write(key.getJoinKey(), new SalaryDepartmentClient((DepartmentClient)(entities.get(0)), (Salary)(entities.get(1))));
        }
    }
}
