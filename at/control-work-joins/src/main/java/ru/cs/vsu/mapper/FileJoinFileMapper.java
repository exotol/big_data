package ru.cs.vsu.mapper;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import ru.cs.vsu.entity.AbstractBaseEntity;
import ru.cs.vsu.entity.Salary;
import ru.cs.vsu.key.TaggedKey;
import ru.cs.vsu.util.ParserType;
import ru.cs.vsu.util.Parsers;

import java.io.IOException;

/**
 * Created by maxtrav on 15.11.16.
 */
public class FileJoinFileMapper extends Mapper<LongWritable, Text, TaggedKey, AbstractBaseEntity> {


    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        Salary salary = (Salary) Parsers.getParser(ParserType.SALARY).parse(value.toString());
        if(salary != null) {
            context.write(salary.getId(), salary);
        }
    }


}
