package ru.cs.vsu.key;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by maxtrav on 16.11.16.
 */
public class TaggedKey implements Writable, WritableComparable<TaggedKey> {
    private LongWritable joinKey = new LongWritable();
    private LongWritable tag = new LongWritable();


    public TaggedKey(LongWritable joinKey, LongWritable tag) {
        this.joinKey = joinKey;
        this.tag = tag;
    }

    public TaggedKey(TaggedKey id) {
        this.joinKey = new LongWritable(id.getJoinKey().get());
        this.tag = new LongWritable(id.getTag().get());
    }

    public void set(TaggedKey key) {
        joinKey.set(key.getJoinKey().get());
        tag.set(key.getTag().get());
    }

    @Override
    public int compareTo(TaggedKey o) {
        int compareValue = this.joinKey.compareTo(o.getJoinKey());
        if(compareValue == 0) {
            compareValue = this.tag.compareTo(o.getTag());
        }
        return compareValue;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        joinKey.write(out);
        tag.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        joinKey.readFields(in);
        tag.readFields(in);
    }

    public LongWritable getJoinKey() {
        return joinKey;
    }

    public void setJoinKey(LongWritable joinKey) {
        this.joinKey = joinKey;
    }

    public LongWritable getTag() {
        return tag;
    }

    public void setTag(LongWritable tag) {
        this.tag = tag;
    }
}
