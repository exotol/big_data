package ru.cs.vsu.entity;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import ru.cs.vsu.key.TaggedKey;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by maxtrav on 16.11.16.
 */
public class Department extends AbstractBaseEntity<Department, TaggedKey> {

    public static final long ORDER_INDEX = 0L;

    private Text name;

    public Text getName() {
        return name;
    }

    public void setName(Text name) {
        this.name = name;
    }

    private void set(Text name) {
        this.name = name;
    }

    public Department(String departmentId, String name) {
        super(new TaggedKey(new LongWritable(Long.valueOf(departmentId)), new LongWritable(ORDER_INDEX)));
        set(new Text(name));
    }

    public Department(TaggedKey taggedKey, Text name) {
        super(new TaggedKey(taggedKey));
        set(new Text(name.toString()));
    }

    public Department(LongWritable departmentId, Text name) {
        super(new TaggedKey(departmentId, new LongWritable(ORDER_INDEX)));
        set(new Text(name));
    }

    public Department() {
        super(new TaggedKey(new LongWritable(), new LongWritable(ORDER_INDEX)));
        set(new Text());
    }

    @Override
    public void write(DataOutput out) throws IOException {
        id.write(out);
        name.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        id.readFields(in);
        name.readFields(in);
    }


    @Override
    public String toString() {
        return name.toString();
    }
}
