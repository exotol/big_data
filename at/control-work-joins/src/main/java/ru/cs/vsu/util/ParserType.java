package ru.cs.vsu.util;

/**
 * Created by maxtrav on 16.11.16.
 */
public enum ParserType {
    CLIENT, DEPARTMENT, SALARY
}
