package ru.cs.vsu.mapper;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import ru.cs.vsu.entity.AbstractBaseEntity;
import ru.cs.vsu.entity.Client;
import ru.cs.vsu.entity.Department;
import ru.cs.vsu.entity.DepartmentClient;
import ru.cs.vsu.exception.DistributeCacheException;
import ru.cs.vsu.key.TaggedKey;
import ru.cs.vsu.metadata.ClientMetadata;
import ru.cs.vsu.parser.CommonParser;
import ru.cs.vsu.parser.impl.DepartmentDataParser;
import ru.cs.vsu.util.ParserType;
import ru.cs.vsu.util.Parsers;

import java.io.File;
import java.io.IOException;
import java.net.URI;

/**
 * Created by maxtrav on 15.11.16.
 */
public class FileJoinDistributeCacheMapper extends Mapper<LongWritable, Text, TaggedKey, AbstractBaseEntity> {
    private static final int EMPTY_LIST = 0;
    private ClientMetadata clientMetadata;


    public FileJoinDistributeCacheMapper() {
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        clientMetadata = new ClientMetadata();
        if(context.getCacheFiles() != null && context.getCacheFiles().length > EMPTY_LIST) {
            URI[] files = context.getCacheFiles();
            //Инициализация распределенного кеша
            try{
                clientMetadata.initialize(files[0].toString());
            } catch(Exception exp) {
                throw new DistributeCacheException(exp.getMessage());
            }
        } else {
            throw new DistributeCacheException("Файл распределенного кеша не обнаружен!!!");
        }
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        Department department = (Department)Parsers.getParser(ParserType.DEPARTMENT).parse(value.toString());
        if(department != null) {
            Client client = clientMetadata.getClientInfo(department.getId());
            if(client != null) {
                context.write(department.getId(), new DepartmentClient(client, department));
            }
        }
    }
}
