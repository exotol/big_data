package ru.cs.vsu.entity;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by maxtrav on 16.11.16.
 */
public class DepartmentClient extends Department {
    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public DepartmentClient(Client client, Department department) {
        super(department.getId(), department.getName());
        this.client = new Client(client);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        super.write(out);
        client.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        super.readFields(in);
        client.readFields(in);
    }

    @Override
    public String toString() {
        return super.toString() + AbstractBaseEntity.SEPARATOR_STRING + client.toString();
    }
}
