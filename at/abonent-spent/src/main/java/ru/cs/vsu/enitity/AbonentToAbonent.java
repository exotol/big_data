package ru.cs.vsu.enitity;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by maxtrav on 14.11.16.
 */
public class AbonentToAbonent implements WritableComparable {

    private Text abonentFrom;
    private Text abonentTo;

    public AbonentToAbonent(Text abonentFrom, Text abonentTo) {
        this.abonentFrom = abonentFrom;
        this.abonentTo = abonentTo;
    }

    public AbonentToAbonent() {
    }

    public Text getAbonentFrom() {
        return abonentFrom;
    }

    public void setAbonentFrom(Text abonentFrom) {
        this.abonentFrom = abonentFrom;
    }

    public Text getAbonentTo() {
        return abonentTo;
    }

    public void setAbonentTo(Text abonentTo) {
        this.abonentTo = abonentTo;
    }


    @Override
    public void write(DataOutput dataOutput) throws IOException {
        abonentFrom.write(dataOutput);
        abonentTo.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        abonentFrom.readFields(dataInput);
        abonentTo.readFields(dataInput);
    }

    @Override
    public int compareTo(Object o) {
        AbonentToAbonent other = (AbonentToAbonent)o;
        int result = this.getAbonentFrom().compareTo(other.getAbonentFrom());
        if(result == 0) {
            int result2 = this.getAbonentTo().compareTo(other.getAbonentTo());

        }
        return result;
    }
}
