package ru.cs.vsu;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import ru.cs.vsu.mapper.MaxTemperatureMapper;
import ru.cs.vsu.reduce.MaxTemperatureReducer;

import java.io.IOException;

/**
 * Created by maxtrav on 09.11.16.
 */
public class MainDriver extends Configured implements Tool{


    @Override
    public int run(String[] args) throws Exception {
        if(args.length != 2) {
            System.err.printf("Usage: %s [generic options] <input> <output>\n", getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }
        System.setProperty("hadoop.home.dir", "E:\\ProgramFiles\\hadoop\\hadoop\\bin");
        Job job = new Job(getConf());
        job.setJarByClass(getClass());
        job.setJobName("Max temperature");

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setMapperClass(MaxTemperatureMapper.class);
        job.setCombinerClass(MaxTemperatureReducer.class);
        job.setReducerClass(MaxTemperatureReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new MainDriver(), args);
        System.exit(exitCode);
    }

}
