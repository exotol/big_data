package ru.cs.vsu.parser;

import org.apache.hadoop.io.Text;

/**
 * Created by maxtrav on 12.11.16.
 */
public class NcdcRecordParser {
    private static final int MISSING_VALUE = 9999;

    private String year;
    private int airTemperature;
    private String quality;
    private boolean airTemperatureMalformed;

    public void parse(Text record) {
        parse(record.toString());
    }

    private void parse(String record) {
        year = record.substring(15, 19);
        //Удаляем начальный плюс, несовместимый с parseInt
        if(record.charAt(87) == '+') {
            airTemperature = Integer.parseInt(record.substring(88, 92));
        } else if(record.charAt(87) == '-') {
            airTemperature = Integer.parseInt(record.substring(87, 92));
        } else {
            airTemperatureMalformed = true;
        }
        quality = record.substring(92, 93);
    }


    public boolean isValidTemperature() {
        return airTemperature != MISSING_VALUE && quality.matches("[01459]");
    }

    public String getYear() {
        return year;
    }

    public int getAirTemperature() {
        return airTemperature;
    }

    public boolean isAirTemperatureMalformed() {
        return airTemperatureMalformed;
    }
}
