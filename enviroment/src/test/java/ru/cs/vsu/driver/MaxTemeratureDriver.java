package ru.cs.vsu.driver;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.junit.Test;
import ru.cs.vsu.MainDriver;


/**
 * Created by maxtrav on 12.11.16.
 */
public class MaxTemeratureDriver {

    public static class OutputLogFilter implements PathFilter {
        public boolean accept(Path path) {
            return !path.getName().startsWith("_");
        }
    }

    public void test() throws Exception {
        Configuration configuration = new Configuration();
        configuration.set("fs.default.name", "file:///");
        configuration.set("mapred.job.tracker", "local");
        configuration.set("hadoop.tmp.dir", "E:\\tmp\\mapred\\staging\\maxtrav-259938815\\");

        Path input = new Path("E:\\projects\\big_data\\big_data\\data\\ncdc.txt");
        Path output = new Path("E:\\output");
        FileSystem fs = FileSystem.getLocal(configuration);
        fs.delete(output, true);

        MainDriver driver = new MainDriver();
        driver.setConf(configuration);

        int exitCode = driver.run(new String[]{input.toString(),
                output.toString()
        });
        assertThat(exitCode, is(0));
        checkOutput(configuration, output);
    }


    private void checkOutput(Configuration conf, Path output) throws IOException {
        FileSystem fs = FileSystem.getLocal(conf);
        Path[] outputFiles = FileUtil.stat2Paths(
                fs.listStatus(output, new OutputLogFilter()));
        assertThat(outputFiles.length, is(1));

        BufferedReader actual = asBufferedReader(fs.open(outputFiles[0]));
        BufferedReader expected = asBufferedReader(
                getClass().getResourceAsStream("/src/test/resources/expected.txt"));
        String expectedLine;
        while ((expectedLine = expected.readLine()) != null) {
            assertThat(actual.readLine(), is(expectedLine));
        }
        assertThat(actual.readLine(), nullValue());
        actual.close();
        expected.close();
    }

    private BufferedReader asBufferedReader(InputStream in) throws IOException {
        return new BufferedReader(new InputStreamReader(in));
    }

}
